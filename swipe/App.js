import React, { Component } from 'react'
import { StyleSheet, Text, View, Animated } from 'react-native'

import { Card, Button } from 'react-native-elements'

import Ball from './src/Ball'
import Deck from './src/Deck'

const DATA = [
  { id: 1, text: 'Card #1', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-04.jpg' },
  { id: 2, text: 'Card #2', uri: 'http://www.fluxdigital.co/wp-content/uploads/2015/04/Unsplash.jpg' },
  { id: 3, text: 'Card #3', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-09.jpg' },
  { id: 4, text: 'Card #4', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-01.jpg' },
  { id: 5, text: 'Card #5', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-04.jpg' },
  { id: 6, text: 'Card #6', uri: 'http://www.fluxdigital.co/wp-content/uploads/2015/04/Unsplash.jpg' },
  { id: 7, text: 'Card #7', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-09.jpg' },
  { id: 8, text: 'Card #8', uri: 'http://imgs.abduzeedo.com/files/paul0v2/unsplash/unsplash-01.jpg' },
];

export default class App extends Component {

  constructor(){
    super()
    this.state = {
      data: DATA
    }

    this.reload = this.reload.bind(this)
  }

  reload() {
    console.log("RELOAD@!!")
    this.setState({
      data: DATA
    })
  }

  renderCard(item) {

    return (
      <Card
        key={item.id}
        title={item.text}
        image={{uri: item.uri}}
        style={styles.card}
      >
      <Text style={{marginBottom: 10 }}>
          Some rando text for this card.
        </Text>
        <Button title="Click on this"/>
      </Card>
    )
  }

  

  noMoreCards() {

    return (
      <Card
        title='No more Cards!'
        style={styles.card}
      >
      <Text style={{ marginBottom: 10 }}>
          yes really there are no more cards!!
        </Text>
        <Button title="Reload" onPress={this.reload} />
      </Card>
    )
}

  render() {
    return (
      <View style={styles.container}>
        <Deck 
          data={this.state.data}
          noMoreCards={this.noMoreCards}
          renderCard={this.renderCard}
          reload={this.reload}
        />
        {/* <Ball /> */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
})
