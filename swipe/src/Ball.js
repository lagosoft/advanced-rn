import React, { Component } from 'react';
import { Animated, View } from 'react-native';

class Ball extends Component {
    componentWillMount() {
        this.position = new Animated.ValueXY(0,0)
        Animated.spring(this.position, {
            toValue: {x: 150, y: 300 }
        }).start(() => {
            Animated.spring(this.position, {
                toValue: {x: 100, y: 700 }
           })
        })
    }

    render() {
        return (
            <Animated.View style={this.position.getLayout()}>
                <View style={styles.ball} />
            </Animated.View>
        )
    }
}

const styles = {
    ball: {
        height: 60,
        width: 60,
        borderRadius: 30,
        borderWidth: 10,
        borderColor: 'rebeccapurple',
        backgroundColor: 'darkorange'
    }
};

export default Ball;