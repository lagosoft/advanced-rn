import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import firebase from 'firebase';

import SignupForm from './components/SignupForm';
import SigninForm from './components/SigninForm';

export default class App extends React.Component {

  componentDidMount() {
    const config = {
      apiKey: "AIzaSyDmJGZ4KbKY1ChVgNkBmbimJT8X15_unM8",
      authDomain: "one-time-password-bf6b2.firebaseapp.com",
      databaseURL: "https://one-time-password-bf6b2.firebaseio.com",
      projectId: "one-time-password-bf6b2",
      storageBucket: "one-time-password-bf6b2.appspot.com",
      messagingSenderId: "879136434881"
    };
    firebase.initializeApp(config);
  }

  render() {
    return (
      <View style={styles.container}>
        <SigninForm/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
