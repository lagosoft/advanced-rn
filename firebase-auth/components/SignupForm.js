import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { 
    FormLabel,
    FormInput,
    Button
} from 'react-native-elements';

import axios from 'axios';

const rootURL = 'https://us-central1-one-time-password-bf6b2.cloudfunctions.net/';
const createUserURL = rootURL + 'createUser';
const reqOneTimePswdURL = rootURL + 'requestOneTimePswd';

class SignupForm extends Component {
    
    // take advantage of ES2017 and put this directly on the component
    // not through a constructor
    state = {
        phone: ''
    };

    // ES2017: avoid having to use .bind(this) for callbacks.
    handleSubmit = async () => {
        try {
            await axios.post(createUserURL, {phone: this.state.phone});
            await axios.post(reqOneTimePswdURL, { phone: this.state.phone });
        } catch (err) {
            console.log("ERROR: ", err);
        }
    }

    render () {
        return (
            <View>
                <View style={{marginBottom: 10}}>
                    <FormLabel>Enter Phone Number</FormLabel>
                    <FormInput
                        value={this.state.phone}
                        onChangeText={phone => this.setState({ phone })}
                    />
                </View>
                <Button 
                    title="Sign Up"
                    onPress={this.handleSubmit}
                />
            </View>
        );
    }
}

export default SignupForm;