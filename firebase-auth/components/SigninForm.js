import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { 
    FormLabel,
    FormInput,
    Button
} from 'react-native-elements';
import firebase from 'firebase';

import axios from 'axios';

const rootURL = 'https://us-central1-one-time-password-bf6b2.cloudfunctions.net/';
const verifyOneTimePswdURL = rootURL + 'verifyOneTimePswd';

class SigninForm extends Component {
    
    // take advantage of ES2017 and put this directly on the component
    // not through a constructor
    state = {
        phone: '',
        code: ''
    };

    // ES2017: avoid having to use .bind(this) for callbacks.
    handleSubmit = async () => {
        const { phone, code } = this.state;
        try {
            const {data} = await axios.post(verifyOneTimePswdURL, {phone, code});
            console.log("GOT TOKEN : ", data.token);

            firebase.auth().signInWithCustomToken(data.token);
            console.log("LOGGED IN!");
        } catch (err) {
            console.log("ERROR: ", err);
        }
    }

    render () {
        return (
            <View>
                <View style={{marginBottom: 10}}>
                    <FormLabel>Phone Number</FormLabel>
                    <FormInput
                        value={this.state.phone}
                        onChangeText={phone => this.setState({ phone })}
                    />
                    <FormLabel>Verify Code</FormLabel>
                    <FormInput
                        value={this.state.code}
                        onChangeText={code => this.setState({ code })}
                    />
                </View>
                <Button 
                    title="Sign In"
                    onPress={this.handleSubmit}
                />
            </View>
        );
    }
}

export default SigninForm;