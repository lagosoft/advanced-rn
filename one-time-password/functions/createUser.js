const admin = require('firebase-admin');

module.exports = function(req, res) {
    // res.send(req.body);

    // verify the user provided phone
    if (!req.body.phone) {
        return res.status(422).send({ error: 'Bad Input' });
    }

    // format the phone number to remove dashes, parenthesis
    const phone = String(req.body.phone).replace(/[^\d]/g, '');

    // create new user account using that phone #
    admin.auth().createUser({uid: phone})
        .then(user => res.send(user))
        .catch(err => res.status(422).send({ error: err }));

    // respond to the user indicating account was created
}