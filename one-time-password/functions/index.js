const admin = require('firebase-admin');
const functions = require('firebase-functions');
const createUser = require('./createUser');
const serviceAccount = require('./serviceAccount.json');
const requestOneTimePswd = require('./requestOneTimePswd');
const verifyOneTimePswd = require('./verifyOneTimePswd');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://one-time-password-bf6b2.firebaseio.com"
  });

exports.createUser = functions.https.onRequest(createUser);
exports.requestOneTimePswd = functions.https.onRequest(requestOneTimePswd);
exports.verifyOneTimePswd = functions.https.onRequest(verifyOneTimePswd);