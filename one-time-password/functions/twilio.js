const twilio = require('twilio');
const acctSID = require('./twilioSecret').accountSID;
const acctToken = require('./twilioSecret').accountToken;

module.exports = new twilio.Twilio(acctSID, acctToken);
