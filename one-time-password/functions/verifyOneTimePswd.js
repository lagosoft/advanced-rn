const admin = require('firebase-admin');

module.exports = function(req, res) {
    if(!(req.body.phone || req.body.code)) {
        return res.status(422).send({error: 'Phone and code must be provided'});
    }

    // use regex to remove any non-digit characters
    const phone = String(req.body.phone).replace(/[^\d]/g, '');
    const code = parseInt(req.body.code);

    admin.auth().getUser(phone)
        .then(() => {
            const ref = admin.database().ref(`users/${phone}`);
            return ref.on('value', snapshot => {
                ref.off();
                const user = snapshot.val();
                if(!user.codeValid || code !== user.code) {
                    return res.status(422).send({error: 'This code is invalid.'})
                }

                // code valid so we update on database.
                ref.update({codeValid: false});

                admin.auth().createCustomToken(phone)
                    .then(token => res.send({token}))
                    .catch(error => res.status(500).send({error}));
            });
        })
        .catch((error) => res.status(422).send({error}));
}