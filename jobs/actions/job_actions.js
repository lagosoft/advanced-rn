import axios from 'axios';
import reverseGeocode from 'latlng-to-zip';
import qs from 'qs';

import {  FETCH_JOBS, LIKE_JOB, CLEAR_LIKED_JOBS } from './types';

// indeed.com query params
const JOB_QUERY_PARAMS = {
    publisher: '42017388038161157',
    format: 'json',
    v: '2',
    latlong: 1, // get back lat and long itude for jobs
    radius: 25, // in miles
    q: 'javascript' // query

};

const FAKE_JOBS = [
    {
        jobkey: 'abc',
        jobtitle: 'Mil usos',
        jobdescription: 'Hay que saber de todo, cerotes!',
        longitude: -122,
        latitude: 37,
        url: 'https://www.indeed.com/viewjob?jk=f5c215260b66efa5&tk=1c9nvonrpbs0k83n&from=serp&alid=3&advn=2778267634654445'

    }
]
const JOB_ROOT_URL = 'http://api.indeed.com/ads/apisearch?';
const buildJobsUrl = (zip) => {
    const query = qs.stringify({ ...JOB_QUERY_PARAMS, l: zip });
    return JOB_ROOT_URL + query;
}

export const fetchJobs = (region, callback) => async (dispatch) => {
    try {
        let zip = await reverseGeocode(region);
        const url = buildJobsUrl(zip);
        let { data } = await axios.get(url);
        console.log("Datas", data);
        dispatch({ type: FETCH_JOBS, payload: {jobs: FAKE_JOBS} });
        callback();
    } catch(e) {
        console.error(e);
    }
};

export const likeJob = (job) => {
    return {
        type: LIKE_JOB,
        payload: job
    };
};

export const clearLikedJobs = (callback) => (dispatch) => {
    dispatch({ 
        type: CLEAR_LIKED_JOBS, 
        payload: [] 
    });
    callback();
}