import { AsyncStorage } from 'react-native';
import { Facebook } from 'expo';

import {
    FACEBOOK_LOGIN_SUCCESS,
    FACEBOOK_LOGIN_FAIL,
} from './types';

export const fbAppID = '883074175203023';
export const fbTokenAsyncStorageKey = 'fvc:jobs:fb_token';

export const facebookLogin = () => async (dispatch) => {
    let token = await AsyncStorage.getItem(fbTokenAsyncStorageKey);
    if (token) {
        console.log("COOL WE FOUND TOKEN :" , token);
        dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: token });
    } else {
        doFacebookLogin(dispatch);
    }
};

const doFacebookLogin = async (dispatch) => {
    let {type, token} = await Facebook.logInWithReadPermissionsAsync(fbAppID, {
        permissions: ['public_profile'],
    });

    if (type === 'cancel') {
        return dispatch({ type: FACEBOOK_LOGIN_FAIL });
    }

    await AsyncStorage.setItem(fbTokenAsyncStorageKey, token);
    dispatch({ type: FACEBOOK_LOGIN_SUCCESS, payload: token });
};