import React, { Component } from 'react';
import { Platform, View, Text } from 'react-native';
import { connect } from 'react-redux';

import { MapView } from 'expo';
import { Card, Button } from 'react-native-elements';

import Swipe from '../components/Swipe';
import * as actions from '../actions';

class DeckScreen extends Component {

    renderCard(job) {

        const { jobtitle, longitude, latitude, company, formattedRelativeTime, jobdescription } = job;

        const initialRegion = {
            longitude: longitude,
            latitude: latitude,
            latitudeDelta: 0.0045,
            longitudeDelta: 0.02
        }

        return(
            <Card title={jobtitle}>
                <View style={{ height: 200 }}>
                    <MapView
                        scrollEnabled={false}
                        style={{flex: 1}}
                        cacheEnabled={Platform.OS === 'android'}
                        initialRegion={initialRegion}
                    >
                    </MapView>
                </View>
                <View style={styles.detailWrapper}>
                    <Text>{company}</Text>
                    <Text>{formattedRelativeTime}</Text>
                </View>
                <Text>
                    {jobdescription.replace(/<b>/g, '').replace(/<\/b/g, '')}
                </Text>
            </Card>
        );
    }

    noMoreCards = () => {
        return(
            <Card title="No More Jobs">
                <Button
                     title="Back to Map"
                     large
                     icon={{ name: 'my-location'}}
                     backgroundColor="#03A9F4"
                     onPress={() => this.props.navigation.navigate('map')}
                >
                </Button>
            </Card>
        );
    }

    render() {
        return (
            <View style={{ marginTop: 20}}>
                <Swipe
                    data={this.props.jobs}
                    renderCard={this.renderCard}
                    noMoreCards={this.noMoreCards}
                    onSwipeRight={job => this.props.likeJob(job)}  //assume we'll get an action creator called likeJob
                    keyProp='jobkey'
                />
            </View>
        );
    }
}

function mapStateToProps({ jobs }) {
    console.log("DECKSCREEN map TO STATE = ", jobs);
    return jobs;
}

const styles = {
    detailWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginBottom: 10
    }
}

export default connect(mapStateToProps, actions)(DeckScreen);