import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { Button } from 'react-native-elements';
import { connect } from 'react-redux';
import * as actions from '../actions';

class SettingsScreen extends Component {

    onButtonPress = () => {
        this.props.clearLikedJobs(() => {
            this.props.navigation.navigate('map');
        });
    }

    render() {
        console.log(" SETTINGS SCREEN PROPS = ", this.props);
        return (
            <View>
                <Button
                    large
                    title="Clear Liked Jobs"
                    onPress={this.onButtonPress}
                    icon={{name: 'delete-forever'}}
                >
                </Button>
            </View>
        );
    }
}

export default connect(null, actions) (SettingsScreen);