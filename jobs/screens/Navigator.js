import { TabNavigator, StackNavigator } from 'react-navigation';

import AuthScreen from './AuthScreen';
import WelcomeScreen from './WelcomeScreen';
import MapScreen from './MapScreen';
import DeckScreen from './DeckScreen';
import ReviewScreen from './ReviewScreen';
import SettingsScreen from './SettingsScreen';

export default MainNavigator = TabNavigator(
    {
        welcome: { screen: WelcomeScreen },
        auth: { screen: AuthScreen },
        main: {
            screen: TabNavigator({
                map: MapScreen,
                deck: DeckScreen,
                review: {
                    screen: StackNavigator({
                        review: { screen: ReviewScreen },
                        settings: { screen: SettingsScreen }
                    })
                }
            })
        }
    },
    {
        navigationOptions: {
            tabBarVisible: false
        }
    });