import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { AsyncStorage } from 'react-native';

import * as actions from '../actions';

class AuthScreen extends Component {

    async componentDidMount() {
        this.props.facebookLogin();
        this.onAuthComplete(this.props);

        // TODO: Remove before MVP1
        // UNCOMMENT this to clear the asyncstorage:
        // await AsyncStorage.removeItem(actions.fbTokenAsyncStorageKey);
    }

    // we need to do this for when the user goes through the initial login
    async componentWillReceiveProps(nextProps) {
        await this.onAuthComplete(nextProps);
    }

    onAuthComplete(props) {
        if (props.token) {
            this.props.navigation.navigate('map');
        }
    }

    render() {
        return (
            <View/>
        );
    }
}

function mapStateToProps({ auth }) {
    return { token: auth.token };
}

export default connect(mapStateToProps, actions)(AuthScreen);