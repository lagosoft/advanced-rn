import _ from 'lodash';
import React, { Component } from 'react';
import { AsyncStorage, View, Text } from 'react-native';
import { AppLoading } from 'expo';

import * as actions from '../actions';
import Slides from '../components/Slides';

const SLIDE_DATA = [
    { text: 'Welcome to Job App!', color: 'green' },
    { text: 'Use this App to get a new Job!', color: 'rebeccapurple' },
    { text: 'Set your location, then swipe away!', color: 'teal' }
];

class WelcomeScreen extends Component {

    state = { token: null };

    async componentWillMount() {
        let token = await AsyncStorage.getItem(actions.fbTokenAsyncStorageKey);
        if (token) {
            this.props.navigation.navigate('map');
        } else {
            this.setState({ token: false});
        }
    }

    onSlidesComplete = () => {
        this.props.navigation.navigate('auth');
    }

    render() {
        if (_.isNull(this.state.token)) {
            return <AppLoading />
            console.log("lodash says NULL");
        }

        return (
            <View style={{flex: 1}}>
                <Slides 
                    data={SLIDE_DATA}
                    onComplete={this.onSlidesComplete}
                />
            </View>
        );
    }
}

export default WelcomeScreen;