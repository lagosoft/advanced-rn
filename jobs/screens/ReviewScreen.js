import React, { Component } from 'react';
import { View, Text, Platform, ScrollView, Linking } from 'react-native';
import { Card, Button } from 'react-native-elements';
import { MapView } from 'expo';

import { connect } from 'react-redux';

class ReviewScreen extends Component {

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Review Jobs',
            headerRight: (
                <Button 
                    title="Settings" 
                    backgroundColor="rgba(0,0,0,0)"
                    color="rgba(0, 122, 255, 1)"
                    onPress={() => navigation.navigate('settings')} 
                />
            ),
            style: {
                marginTop: Platform.OS === 'android' ? 24 : 0
            }
        }
    };

    renderLikedJobs() {
        return this.props.likedJobs.map(job => {
            const { jobtitle, jobdescription, url, longitude, latitude, jobkey } = job;
            const initialRegion = {
                longitude,
                latitude,
                latitudeDelta: 0.0045,
                longitudeDelta: 0.02
            }
            return (
                <Card title={jobtitle} key={jobkey}>
                    <View style={{height: 200}}>
                        <MapView
                            scrollEnabled={false}
                            style={{flex: 1}}
                            cacheEnabled={Platform.OS === 'android'}
                            initialRegion={initialRegion}
                        >
                        </MapView>
                        <View style={styles.detailWrapper}>
                            <Text style={styles.italics}>{jobdescription}</Text>
                        </View>
                        <Button
                            title="Apply Now!"
                            backgroundColor="#03A9F4"
                            onPress={() => Linking.openURL(url)}
                            icon={{ name: 'edit' }}
                        >
                        </Button>
                    </View>
                </Card>
            );
        });
    }

    render() {
        return (
            <ScrollView>
                {this.renderLikedJobs()}
            </ScrollView>
        );
    }
}

const styles = {
    detailWrapper: {
        marginTop: 10,
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    italics: {
        fontStyle: 'italic'
    }
};

function mapStateToProps(state) {
    return { likedJobs: state.likedJobs };
}

export default connect(mapStateToProps)(ReviewScreen);