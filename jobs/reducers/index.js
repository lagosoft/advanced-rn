import { combineReducers } from 'redux';
import auth from './auth_reducer';
import jobs from './jobs_reducer';
import likedJobs from './like_job_reducer';
import clearLikedJobs from './clear_liked_jobs_reducer';

export default combineReducers({
    auth,
    jobs,
    likedJobs
});