import React, { Component } from 'react';
import { View, Text, ScrollView, Dimensions } from 'react-native';
import { Button } from 'react-native-elements';
// import { LinearGradient } from 'expo';

const SCREEN_WIDTH = Dimensions.get('window').width;

class Slides extends Component {

    renderSlides() {
        console.log(this.props.data);
        return this.props.data.map((slide, index) => {
            return( 
                // <LinearGradient
                //     colors={['#33ffbb', '#cc66ff']}
                // >
                    <View 
                        key={slide.text} 
                        style={[styles.slideStyle, {backgroundColor: slide.color}]}>
                            <Text style={styles.textStyle}>{slide.text}</Text>
                            { index === this.props.data.length-1 
                                ? <Button 
                                    buttonStyle={styles.buttonStyle} 
                                    textStyle={{fontStyle: 'italic'}}
                                    title='Continue' 
                                    onPress={this.props.onComplete}
                                  />
                                : null }
                    </View>
                // </LinearGradient>
            );
        });
    }

    render() {
        return (
            <ScrollView 
                horizontal
                pagingEnabled
                style={{flex:1}}
            >
                { this.renderSlides() }
            </ScrollView>
        );

    }
}

const styles = {
    slideStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        width: SCREEN_WIDTH,
        padding: 10
    },
    textStyle: {
        fontSize: 30,
        color: 'white',
        fontWeight: '800'
    },
    buttonStyle: {
        backgroundColor: 'rgba(0,0,0,0.2)',
        marginTop: 15
    }
};

export default Slides;