import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';

import MainNavigator from './screens/Navigator';
import store from './store';

export default class App extends React.Component {
  render() {

    return (
      <Provider store={store}>
        <MainNavigator style={styles.container}/>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
